﻿using BEAM_Projector.Components;
using Microsoft.Win32.TaskScheduler;
using Newtonsoft.Json;
using Projectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

namespace BEAM_Projector
{
    public partial class MainWindow : Window
    {
        private NotifyIcon notifyIcon = new NotifyIcon();
        public static schedulePage schedulepage;
        public settingPage settingpage;
        private static IniFile file;
        private static Dictionary<string, Projector> dic = new Dictionary<string, Projector>();
        private System.Timers.Timer timer = new System.Timers.Timer();

        public MainWindow()
        {

            createScheduleFile();

            InitializeComponent();

            initializeNotifyicon();
            settingpage = new settingPage(this);
            schedulepage = new schedulePage(this);

            timerCheck();
            pageSwitching.Navigate(settingpage);

            this.Hide();

            littleHeader.Text = "Set up your daily scheduler";

            startupProjector();
        }

        private static bool taskexistance(string taskname)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = "schtasks.exe"; // Specify exe name.
            start.UseShellExecute = false;
            start.CreateNoWindow = true;
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.Arguments = "/query /TN " + taskname;
            start.RedirectStandardOutput = true;
            // Start the process.
            using (Process process = Process.Start(start))
            {
                // Read in all the text from the process with the StreamReader.
                using (StreamReader reader = process.StandardOutput)
                {
                    string stdout = reader.ReadToEnd();
                    if (stdout.Contains(taskname))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        static void startupProjector()
        {

            if (taskexistance("BEAM") == false)
            {
                if (IsAdministrator() == false)
                {
                    // Restart program and run as admin
                    var exeName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                    ProcessStartInfo startInfo = new ProcessStartInfo(exeName);
                    startInfo.Verb = "runas";
                    System.Diagnostics.Process.Start(startInfo);
                    return;
                }

                using (TaskService ts = new TaskService())
                {
                    // Create a new task definition and assign properties
                    TaskDefinition td = ts.NewTask();

                    // td.RegistrationInfo.Description = "sapir was here";
                    td.RegistrationInfo.Author = "EyeClick";

                    td.Triggers.Add(new BootTrigger());
                    td.Triggers.Add(new LogonTrigger());

                    td.Actions.Add(new ExecAction(@"C:\BEAM\Projectors\Resources\Projector.bat", null, @"C:\BEAM\Projectors\Resources"));
                    td.Actions.Add(new ExecAction(@"C:\BEAM\Projectors\Resources\AvatarWallLauncher.bat", null, @"C:\BEAM\Projectors\Resources"));
                    Console.WriteLine("\r\n" + td.XmlText + "\r\n");


                    // Register the task in the root folder
                    try
                    {
                        ts.RootFolder.RegisterTaskDefinition("BEAM", td);

                        Console.WriteLine("Success");
                    }
                    catch
                    {
                        Console.WriteLine("Failed");
                    }

                }
            }
        }

        private static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }


        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            List<Schedule> list = getTodayScheduling();
            string nowtime = DateTime.Now.ToString("HH:mm");
            string reboottime = file.readString("SETTINGS", "rebootHour", "23") + ":" + file.readString("SETTINGS", "rebootMinute", "00");

            foreach (var item in list)
            {
                if (item.ontime == nowtime)
                {
                    foreach (var projector in dic.Values)
                    {
                        writeLog("trying to turn on " + projector.GetType() + "(" + projector.getPort() + ") projector");
                        projector.TurnOn();
                        file.writeBool("SETTINGS", "isOn", true);
                    }
                }
                if (item.offtime == nowtime)
                {
                    foreach (var projector in dic.Values)
                    {
                        writeLog("trying to turn off " + projector.GetType() + "(" + projector.getPort() + ") projector");
                        projector.TurnOff();
                        file.writeBool("SETTINGS", "isOn", false);
                    }
                }
            }

            if (nowtime == reboottime && file.readBool("SETTINGS", "rebootIsChecked", true))
            {
                writeLog("restarting computer...");
                Restart();
            }

        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
        private void mainwindow_closed(object sender, EventArgs e)
        {
            if (schedulepage.flagloaded)
                schedulepage.refreshSchedule();

            settingpage.updateRebootTime();

            notifyIcon.Visible = false;
            writeLog("PROJECTOR APP WAS CLOSED");
        }
        private void Schedule_Btn_Click(object sender, RoutedEventArgs e)
        {
            Color color = (Color)ColorConverter.ConvertFromString("#FF0079F1");

            pageSwitching.Navigate(schedulepage);
            littleHeader.Text = "Choose days and hours to run projection";
            Schedule_Btn.Background = Brushes.White;
            Setting_Btn.Background = Brushes.Transparent;

            Schedule_Btn.Foreground = new System.Windows.Media.SolidColorBrush(color);
            Setting_Btn.Foreground = Brushes.White;

            var template1 = Setting_Btn.Template;
            var border1 = (Border)template1.FindName("bdr_main1", Setting_Btn);
            border1.Background = new System.Windows.Media.SolidColorBrush(color);

            var template2 = Schedule_Btn.Template;
            var border2 = (Border)template2.FindName("bdr_main", Schedule_Btn);
            border2.Background = Brushes.White;

        }
        private void Setting_Btn_Click(object sender, RoutedEventArgs e)
        {
            Color color = (Color)ColorConverter.ConvertFromString("#FF0079F1");

            pageSwitching.Navigate(settingpage);
            littleHeader.Text = "Set up your daily scheduler";
            Setting_Btn.Background = Brushes.White;
            Schedule_Btn.Background = Brushes.Transparent;

            Schedule_Btn.Foreground = Brushes.White;
            Setting_Btn.Foreground = new System.Windows.Media.SolidColorBrush(color);

            var template1 = Setting_Btn.Template;
            var border1 = (Border)template1.FindName("bdr_main1", Setting_Btn);
            border1.Background = Brushes.White;

            var template2 = Schedule_Btn.Template;
            var border2 = (Border)template2.FindName("bdr_main", Schedule_Btn);
            border2.Background = new System.Windows.Media.SolidColorBrush(color);

        }
        private void Exit_Btn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult d = System.Windows.Forms.MessageBox.Show("Are you sure you want to exit PROJECTOR? \nscheduling will be turned off.", "", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);

            if (d.ToString() == "Yes")
            {
                this.Close();
                Environment.Exit(0);
            }
        }       
        private void minimize_Btn_Click(object sender, RoutedEventArgs e)
        {
            if (schedulepage.flagloaded)
                schedulepage.refreshSchedule();

            settingpage.updateRebootTime();

            this.Visibility = Visibility.Hidden;
       //     this.WindowState = WindowState.Minimized;

            writeLog("minimized...");
        }
        private void notifyIcon_Click(object sender, EventArgs e)
        {
            this.Visibility = Visibility.Visible;
            this.WindowState = WindowState.Normal;
            this.Focus();
            this.Show();
        }

        private void initializeNotifyicon()
        {
            notifyIcon.Icon = BEAM_Projector.Properties.Resources.beam_projector_icon;
            notifyIcon.Visible = true;
            notifyIcon.Click += new EventHandler(notifyIcon_Click);
        }
        private static void Restart()
        {
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.WindowStyle = ProcessWindowStyle.Hidden;
            proc.FileName = "cmd";
            proc.Arguments = "/C shutdown -f -r -t 00";
            Process.Start(proc);
        }
        private static List<Schedule> getEachDaySchedule(int j, string[,] arr)
        {
            Schedule schedule = new Schedule();
            List<Schedule> times = new List<Schedule>();

            bool flag = false;

            for (int i = 0; i < schedulepage.getNumOfItems() - 1; i++)
            {
                if (arr[i, j] == "YAP")
                    if (!flag)
                    {
                        flag = true;
                        schedule.ontime = arr[i, 0];
                    }
                if (arr[i, j] == null)
                    if (flag)
                    {
                        flag = false;
                        schedule.offtime = arr[i, 0];
                        times.Add(schedule);
                        schedule = new Schedule();
                    }
            }
            if (flag)
            {
                schedule.offtime = "00:00";
                times.Add(schedule);
            }

            return times;
        }
        private static List<Schedule> getTodayScheduling()
        {
            List<Schedule> times = new List<Schedule>();
            if (File.Exists(@"C:\BEAM\Projectors\Settings\ARR.txt"))
            {
                var str = System.IO.File.ReadAllText(@"C:\BEAM\Projectors\Settings\ARR.txt");
                var arr = JsonConvert.DeserializeObject<string[,]>(str);

                switch (DateTime.Today.DayOfWeek.ToString())
                {
                    case "Sunday":
                        times = getEachDaySchedule(1, arr);
                        break;
                    case "Monday":
                        times = getEachDaySchedule(2, arr);
                        break;
                    case "Tuesday":
                        times = getEachDaySchedule(3, arr);
                        break;
                    case "Wednesday":
                        times = getEachDaySchedule(4, arr);
                        break;
                    case "Thursday":
                        times = getEachDaySchedule(5, arr);
                        break;
                    case "Friday":
                        times = getEachDaySchedule(6, arr);
                        break;
                    case "Saturday":
                        times = getEachDaySchedule(7, arr);
                        break;

                }
            }
            var str_times = JsonConvert.SerializeObject(times);
            file.writeString("SETTINGS", "toady_schedule", str_times);
            return times;
        }

        private void timerCheck()
        {
            timer.Interval = 30000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        public void addToDictionary(string info, Projector projector)
        {
            try
            {
                dic.Add(info, projector);
                writeLog("a projector was added. type: " + projector.GetType() + " on port:" + projector.getPort());
            }
            catch (Exception e)
            {
                writeLog("couldn't add projector " + e.ToString()); 
            }
        }
        public void deleteFromDictionary(string info)
        {
            try
            {
                dic.Remove(info);
                writeLog("a projector was removed. " + info);
            }
            catch (Exception e)
            {
                writeLog("couldn't remove projector " + e.ToString());
            }
        }
        public Projector getDictionaryValue(string info)
        {
                return dic[info];
        }
        public int getNumOfProjectors()
        {
            return dic.Count();
        }

        public static byte[] ExtractResource(String filename)
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            using (Stream resFilestream = a.GetManifestResourceStream(filename))
            {
                if (resFilestream == null) return null;
                byte[] ba = new byte[resFilestream.Length];
                resFilestream.Read(ba, 0, ba.Length);
                return ba;
            }
        }

        public void createScheduleFile()
        {


            if (!File.Exists(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini"))
            {
                if (!Directory.Exists(@"C:\BEAM\Projectors"))
                {
                    Directory.CreateDirectory(@"C:\BEAM\Projectors");
                }

                if (!Directory.Exists(@"C:\BEAM\Projectors\Settings"))
                {
                    Directory.CreateDirectory(@"C:\BEAM\Projectors\Settings");
                }

                writeLog("creating 'projectorScheduling.ini' file");
                file = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");

                file.writeInt("SETTINGS", "interval", 60);
                file.writeInt("SETTINGS", "startTime", 8);
                file.writeInt("SETTINGS", "rebootHour", 23);
                file.writeInt("SETTINGS", "rebootMinute", 0);
                file.writeBool("SETTINGS", "isOn", false);

                file.writeString("PROJECTORS", "BENQ", "0");
                file.writeString("PROJECTORS", "DIGITALPROJECTION", "0");
                file.writeString("PROJECTORS", "EPSON", "0");
                file.writeString("PROJECTORS", "PANASONIC", "0");
                file.writeString("PROJECTORS", "SANYO", "0");
                file.writeString("PROJECTORS", "SHARP", "0");


            }
            else
                file = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");
        }
        public static void writeLog(string message)
        {
            File.AppendAllText("C:/BEAM/Projectors/Settings/log.txt", DateTime.Now.Date.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToString("HH:mm:ss") + " :" + message + "\n\r\n");
        }        
    }

    public class Schedule
    {
        public string ontime { get; set; }
        public string offtime { get; set; }

        public Schedule()
        {
            offtime = "00:00";
        }
    }

}
