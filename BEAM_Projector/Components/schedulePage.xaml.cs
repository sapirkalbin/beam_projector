﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using Newtonsoft.Json;

namespace BEAM_Projector.Components
{
    public partial class schedulePage : Page
    {
        private MainWindow mainwindow;
        private int interval,startHour;
        private List<RowInfo> rowinfo_datagrid_list;
        private string[,] arr;
        public bool flagloaded = false;

        public schedulePage(MainWindow mainwindow)
        {
            InitializeComponent();
            this.mainwindow = mainwindow;
            getScheduleInfo();
            dataGrid.ItemsSource = setTableContent(false);
            arr = new string[dataGrid.Items.Count, dataGrid.Columns.Count];
            dataGrid.CanUserReorderColumns = false;
        }

        private void schedulePage_Loaded(object sender, RoutedEventArgs e)
        {
            flagloaded = true;
            dataGrid.FontSize = 12;

            if (File.Exists(@"C:\BEAM\Projectors\Settings\ARR.txt"))
            {
                var str = System.IO.File.ReadAllText(@"C:\BEAM\Projectors\Settings\ARR.txt");
                arr = JsonConvert.DeserializeObject<string[,]>(str);

            }

            if (interval.ToString() == mainwindow.settingpage.timeIntervalCmbx.SelectedItem.ToString().Substring(0, 2)
            && startHour == findHour())
            {
                if (!File.Exists(@"C:\BEAM\Projectors\Settings\listOfRowData.txt"))
                {
                    getScheduleInfo();
                    dataGrid.ItemsSource = setTableContent(false);
                    mainwindow.createScheduleFile();
                    arr = new string[dataGrid.Items.Count - 1, dataGrid.Columns.Count];
                    var json = JsonConvert.SerializeObject(arr);
                    File.WriteAllText(@"C:\BEAM\Projectors\Settings\ARR.txt", json);
                    writeLog("interval.ToString() == mainwindow.settingpage.timeIntervalCmbx.SelectedItem.ToString().Substring(0, 2) && startHour == findHour()");
                }
                else
                {
                    try
                    {
                        var str = System.IO.File.ReadAllText(@"C:\BEAM\Projectors\Settings\listOfRowData.txt");
                        rowinfo_datagrid_list = (List<RowInfo>)JsonConvert.DeserializeObject<List<RowInfo>>(str);
                        str = System.IO.File.ReadAllText(@"C:\BEAM\Projectors\Settings\ARR.txt");
                    }

                    catch
                    {

                    }



                    dataGrid.ItemsSource = setTableContent(true);

                    for (int i = 0; i < dataGrid.Items.Count - 1; i++)
                        for (int j = 0; j < dataGrid.Columns.Count; j++)
                        {
                            if (arr[i, j] == "YAP")
                            {
                                SelectCellByIndex(dataGrid, i, j);
                            }
                        }

                }

            }
            else
            {
                File.Delete(@"C:\BEAM\Projectors\Settings\listOfRowData.txt");
                File.Delete(@"C:\BEAM\Projectors\Settings\ARR.txt");
                getScheduleInfo();
                dataGrid.ItemsSource = setTableContent(false);
                mainwindow.createScheduleFile();
                arr = new string[dataGrid.Items.Count, dataGrid.Columns.Count];

            }


        }
        private void schedulePage_Unloaded(object sender, RoutedEventArgs e)
        {
            refreshSchedule();
        }
        private void datagrid_mouseLeftBtnUp(object sender, MouseButtonEventArgs e)
        {
            selectionChanged();
        }
        private void clearBtn_Click(object sender, RoutedEventArgs e)
        {
            File.Delete(@"C:\BEAM\Projectors\Settings\listOfRowData.txt");
            File.Delete(@"C:\BEAM\Projectors\Settings\ARR.txt");
            getScheduleInfo();
            dataGrid.ItemsSource = setTableContent(false);
            mainwindow.createScheduleFile();
            arr = new string[dataGrid.Items.Count - 1, dataGrid.Columns.Count];
            writeLog("all previous schedule was deleted");
        }
        private void getScheduleInfo()
        {
            if (mainwindow.settingpage.timeIntervalCmbx.SelectedItem.ToString() == "15 MIN")
                interval = 15;
            if (mainwindow.settingpage.timeIntervalCmbx.SelectedItem.ToString() == "20 MIN")
                interval = 20;
            if (mainwindow.settingpage.timeIntervalCmbx.SelectedItem.ToString() == "30 MIN")
                interval = 30;
            if (mainwindow.settingpage.timeIntervalCmbx.SelectedItem.ToString() == "60 MIN")
                interval = 60;

            this.startHour = findHour();

        }
        private int findHour()
        {
            if (mainwindow.settingpage.scheduleStartCmbx.SelectedItem.ToString().ElementAt(0) == '0')
                return Convert.ToInt32(mainwindow.settingpage.scheduleStartCmbx.SelectedItem.ToString().ElementAt(1).ToString());
            else
                return Convert.ToInt32(mainwindow.settingpage.scheduleStartCmbx.SelectedItem.ToString().Substring(0, 2));
        }
        private void selectionChanged()
        {
            try {
                DataGrid grid = dataGrid as DataGrid;

                var selectedcells = grid.SelectedCells;

                foreach (var cell in selectedcells)
                {
                    DataGridCell _cell = GetDataGridCell(cell);
                    string day = cell.Column.Header.ToString();
                    if (_cell != null)
                    {
                        if (_cell.Background != Brushes.SkyBlue && day != "TIME")
                        {
                            _cell.Background = Brushes.SkyBlue;
                            _cell.IsSelected = true;
                            if (day == "SUNDAY")
                                ((RowInfo)cell.Item).SUNDAY.isSelected = true;
                            if (day == "MONDAY")
                                ((RowInfo)cell.Item).MONDAY.isSelected = true;
                            if (day == "TUESDAY")
                                ((RowInfo)cell.Item).TUESDAY.isSelected = true;
                            if (day == "WEDNESDAY")
                                ((RowInfo)cell.Item).WEDNESDAY.isSelected = true;
                            if (day == "THURSDAY")
                                ((RowInfo)cell.Item).THURSDAY.isSelected = true;
                            if (day == "FRIDAY")
                                ((RowInfo)cell.Item).FRIDAY.isSelected = true;
                            if (day == "SATURDAY")
                                ((RowInfo)cell.Item).SATURDAY.isSelected = true;
                        }
                        else
                        {
                            _cell.Background = Brushes.White;
                            _cell.IsSelected = false;
                            if (day == "SUNDAY")
                                ((RowInfo)cell.Item).SUNDAY.isSelected = false;
                            if (day == "MONDAY")
                                ((RowInfo)cell.Item).MONDAY.isSelected = false;
                            if (day == "TUESDAY")
                                ((RowInfo)cell.Item).TUESDAY.isSelected = false;
                            if (day == "WEDNESDAY")
                                ((RowInfo)cell.Item).WEDNESDAY.isSelected = false;
                            if (day == "THURSDAY")
                                ((RowInfo)cell.Item).THURSDAY.isSelected = false;
                            if (day == "FRIDAY")
                                ((RowInfo)cell.Item).FRIDAY.isSelected = false;
                            if (day == "SATURDAY")
                                ((RowInfo)cell.Item).SATURDAY.isSelected = false;
                        }
                    }

                }
            }
            catch
            {

            }
            // getDatagrid(dataGrid);
        }
        private static void SelectCellByIndex(DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || rowIndex > (dataGrid.Items.Count - 1))
                throw new ArgumentException(string.Format("{0} is an invalid row index.", rowIndex));

            if (columnIndex < 0 || columnIndex > (dataGrid.Columns.Count - 1))
                throw new ArgumentException(string.Format("{0} is an invalid column index.", columnIndex));

            dataGrid.SelectedCells.Clear();

            object item = dataGrid.Items[rowIndex]; //=Product X
            DataGridRow row = dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow;
            if (row == null)
            {
                dataGrid.ScrollIntoView(item);
                row = dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow;
            }
            if (row != null)
            {
                DataGridCell cell = GetCell(dataGrid, row, columnIndex);
                if (cell != null)
                {
                    DataGridCellInfo dataGridCellInfo = new DataGridCellInfo(cell);
                    dataGrid.SelectedCells.Add(dataGridCellInfo);
                    cell.Background = Brushes.SkyBlue;
                }
            }
        }
        private static T FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is T)
                    return (T)child;
                else
                {
                    T childOfChild = FindVisualChild<T>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        private static DataGridCell GetCell(DataGrid dataGrid, DataGridRow rowContainer, int column)
        {
            if (rowContainer != null)
            {
                DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(rowContainer);
                if (presenter == null)
                {
                    /* if the row has been virtualized away, call its ApplyTemplate() method
                     * to build its visual tree in order for the DataGridCellsPresenter
                     * and the DataGridCells to be created */
                    rowContainer.ApplyTemplate();
                    presenter = FindVisualChild<DataGridCellsPresenter>(rowContainer);
                }
                if (presenter != null)
                {
                    DataGridCell cell = presenter.ItemContainerGenerator.ContainerFromIndex(column) as DataGridCell;
                    if (cell == null)
                    {
                        /* bring the column into view
                         * in case it has been virtualized away */
                        dataGrid.ScrollIntoView(rowContainer, dataGrid.Columns[column]);
                        cell = presenter.ItemContainerGenerator.ContainerFromIndex(column) as DataGridCell;
                    }
                    return cell;
                }
            }
            return null;
        }
        private List<RowInfo> getDatagrid(DataGrid datagrid)
        {

            List<RowInfo> list = new List<RowInfo>();
                for (int i = 0; i < dataGrid.Items.Count-1; i++)
                {
                    for (int j = 0; j < dataGrid.Columns.Count; j++)
                    {
                        var info = new DataGridCellInfo((dataGrid.Items[i]), dataGrid.Columns[j]);
                        string day = info.Column.Header.ToString();

                        if ((RowInfo)info.Item != null)
                        {
                            arr[i, 0] = ((RowInfo)info.Item).TIME;
                            if (day == "SUNDAY")
                                if (((RowInfo)info.Item).SUNDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                            if (day == "MONDAY")
                                if (((RowInfo)info.Item).MONDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                            if (day == "TUESDAY")
                                if (((RowInfo)info.Item).TUESDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                            if (day == "WEDNESDAY")
                                if (((RowInfo)info.Item).WEDNESDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                            if (day == "THURSDAY")
                                if (((RowInfo)info.Item).THURSDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                            if (day == "FRIDAY")
                                if (((RowInfo)info.Item).FRIDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                            if (day == "SATURDAY")
                                if (((RowInfo)info.Item).SATURDAY.isSelected == true)
                                    arr[i, j] = "YAP";
                                else
                                    arr[i, j] = null;
                        }


                    }

                    RowInfo rw = (RowInfo)dataGrid.Items[i];
                    if (rw != null)
                        list.Add(rw);
                }


                var json = JsonConvert.SerializeObject(arr);
                File.WriteAllText(@"C:\BEAM\Projectors\Settings\ARR.txt", json);
            writeLog("getdatagrid function");


            return list;
        }
        private DataGridCell GetDataGridCell(DataGridCellInfo cellInfo)
        {
            var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);
            if (cellContent != null)
                return (DataGridCell)cellContent.Parent;

            return null;
        }
        private List<RowInfo> setTableContent(bool b)
        {
            List<RowInfo> list;
            if (b)
            {
                list = rowinfo_datagrid_list;
            }
            else
            {
                list = new List<RowInfo>();

                RowInfo newinfo = null;
                bool flag = false;

                //if (interval == 15)
                //{
                //    for (int i = startHour , k=1; i < 28; i++)
                //    {
                //        for (int j = 0; j < 4; j++)
                //        {
                //            if (i >= 25)
                //            {
                //                if (j == 0)
                //                    newinfo = new RowInfo { TIME = k + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                if (j == 1)
                //                    newinfo = new RowInfo { TIME = k + ":15", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                if (j == 2)
                //                    newinfo = new RowInfo { TIME = k + ":30", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                if (j == 3)
                //                    newinfo = new RowInfo { TIME = k + ":45", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                k++;
                //            }
                //            else {
                //                if (i > 9)
                //                    flag = true;
                //                if (j == 0)
                //                    newinfo = new RowInfo { TIME = i + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                if (j == 1)
                //                    newinfo = new RowInfo { TIME = i + ":15", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                if (j == 2)
                //                    newinfo = new RowInfo { TIME = i + ":30", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //                if (j == 3)
                //                    newinfo = new RowInfo { TIME = i + ":45", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //            }
                //            list.Add(newinfo);
                //        }

                //    }
                //}

                //if (interval == 20)
                //{
                //    for (double i = startHour; i < 24; i++)
                //    {
                //        for (int j = 0; j < 3; j++)
                //        {
                //            if (i > 9)
                //                flag = true;
                //            if (j == 0)
                //                newinfo = new RowInfo { TIME = i + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //            if (j == 1)
                //                newinfo = new RowInfo { TIME = i + ":20", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                //            if (j == 2)
                //                newinfo = new RowInfo { TIME = i + ":40", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag)};

                //            list.Add(newinfo);
                //        }


                //    }
                //}

                if (interval == 30)
                {
                    for (double i = startHour, k = 1; i < 24 + startHour; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            if (i > 9 && i < 24)
                                flag = true;
                            else
                                flag = false;

                            if (i >= 25)
                            {
                                if (j == 0)
                                    newinfo = new RowInfo { TIME = "0" + k + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                                if (j == 1)
                                    newinfo = new RowInfo { TIME = "0" + k + ":30", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                                
                            }
                            else {
                                if (j == 0)
                                    newinfo = new RowInfo { TIME = i + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                                if (j == 1)
                                    newinfo = new RowInfo { TIME = i + ":30", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                            }
                            list.Add(newinfo);
                        }
                        if(i>=25)
                            k++;    
                    }
                }

                if (interval == 60)
                {
                    for (int i = startHour, k=1; i < 24 + startHour; i++)
                    {
                        if (i > 9 && i < 24)
                            flag = true;
                        else
                            flag = false;

                        if (i >= 25)
                        {
                            newinfo = new RowInfo { TIME = "0" + k + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                            k++;
                        }
                        else
                        {
                            newinfo = new RowInfo { TIME = i + ":00", SUNDAY = new CellInfo(flag), MONDAY = new CellInfo(flag), TUESDAY = new CellInfo(flag), WEDNESDAY = new CellInfo(flag), THURSDAY = new CellInfo(flag), FRIDAY = new CellInfo(flag), SATURDAY = new CellInfo(flag) };
                        }
                        list.Add(newinfo);
                    }
                }

                rowinfo_datagrid_list = list;

            }
            return list;
        }
        public void writeLog(string message)
        {
            File.AppendAllText("C:/BEAM/Projectors/Settings/log.txt", DateTime.Now.Date.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToString("HH:mm:ss") + " :" + message + "\n");
        }
        public int getNumOfItems()
        {
            return dataGrid.Items.Count;
        }
        public void refreshSchedule()
        {
            rowinfo_datagrid_list = getDatagrid(dataGrid);
            var json = JsonConvert.SerializeObject(rowinfo_datagrid_list);
            System.IO.File.WriteAllText(@"C:\BEAM\Projectors\Settings\listOfRowData.txt", json);
        }
    }

    public class RowInfo
    {
        public string Name;
        public string TIME { get; set; }

        public CellInfo SUNDAY { get; set; }
        public CellInfo MONDAY { get; set; }
        public CellInfo TUESDAY { get; set; }
        public CellInfo WEDNESDAY { get; set; }
        public CellInfo THURSDAY { get; set; }
        public CellInfo FRIDAY { get; set; }
        public CellInfo SATURDAY { get; set; }
    }
    public class CellInfo
    {
        public string content;
        public bool isSelected;

        public CellInfo(bool flag)
        {
            content = "";
            isSelected = flag;
        }

        public bool getIsSelected()
        {
            return isSelected;
        }
        public void setIsSelected(bool b)
        {
            isSelected = b;
        }

        public override string ToString()
        {
            return content;
        }

    }

}
