﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Projectors.Components;
using System.IO.Ports;
using Projectors;
using System;

namespace BEAM_Projector.Components
{
    public partial class settingPage : Page
    {
        private MainWindow mainwindow;
        private IniFile ini;

        public settingPage(MainWindow mainwindow)
        {
            InitializeComponent();
            intializeTimeIntervalCmbx();
            intializeProjectorTypeCmbx();
            intializePortCmbx();
            intializeScheduleCmbx();

            this.mainwindow = mainwindow;
            ini = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");

            if (ini != null)
            {
                if (ini.readInt("SETTINGS", "startTime", 8) < 10)
                    scheduleStartCmbx.SelectedItem = "0" + ini.readInt("SETTINGS", "startTime", 8) + ":00";
                else
                    scheduleStartCmbx.SelectedItem = ini.readInt("SETTINGS", "startTime", 8) + ":00";

                timeIntervalCmbx.SelectedItem = ini.readInt("SETTINGS", "interval", 60) + " MIN";

                textBox1.Text = ini.readInt("SETTINGS", "rebootHour", 23).ToString();
                textBox2.Text = ini.readInt("SETTINGS", "rebootMinute", 0).ToString();

                if (ini.readBool("SETTINGS", "rebootIsChecked", true))
                    checkBox.IsChecked = true;
                else
                    checkBox.IsChecked = false;
            }
            else
            {
                scheduleStartCmbx.SelectedItem = "08:00";
                timeIntervalCmbx.SelectedItem = "60 MIN";
            }

        }

        private void listview_Intialize(object sender, RoutedEventArgs e)
        {
            initializeProjectorTable();
        }
        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            ini = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");
            if( (string)portCmbx.SelectedItem != null)
                {
                switch ((string)projectorTypeCmbx.SelectedItem)
                {
                    case "BENQ":
                        addProjectorToTable((string)projectorTypeCmbx.SelectedItem);
                        break;
                    case "EPSON":
                        addProjectorToTable((string)projectorTypeCmbx.SelectedItem);
                        break;
                    case "DIGITALPROJECTION":
                        addProjectorToTable((string)projectorTypeCmbx.SelectedItem);
                        break;
                    case "PANASONIC":
                        addProjectorToTable((string)projectorTypeCmbx.SelectedItem);
                        break;
                    case "SANYO":
                        addProjectorToTable((string)projectorTypeCmbx.SelectedItem);
                        break;
                    case "SHARP":
                        addProjectorToTable((string)projectorTypeCmbx.SelectedItem);
                        break;
                }
            }

        }
        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedItem != null)
            {
                if ((listView.SelectedItem as ProjectorTable).ProjectorType == "SHARP")
                {
                    ini.writeString("PROJECTORS", "SHARP" + (listView.SelectedItem as ProjectorTable).PORT, "0");
                    mainwindow.deleteFromDictionary("SHARP" + (listView.SelectedItem as ProjectorTable).PORT);
                    portCmbx.Items.Add((listView.SelectedItem as ProjectorTable).PORT);
                    listView.Items.Remove((listView.SelectedItem as ProjectorTable));
                }

                else if ((listView.SelectedItem as ProjectorTable).ProjectorType == "SANYO")
                {
                    ini.writeString("PROJECTORS", "SANYO" + (listView.SelectedItem as ProjectorTable).PORT, "0");
                    mainwindow.deleteFromDictionary("SANYO" + (listView.SelectedItem as ProjectorTable).PORT);
                    portCmbx.Items.Add((listView.SelectedItem as ProjectorTable).PORT);
                    listView.Items.Remove((listView.SelectedItem as ProjectorTable));
                }

                else if ((listView.SelectedItem as ProjectorTable).ProjectorType == "PANASONIC")
                {
                    ini.writeString("PROJECTORS", "PANASONIC" + (listView.SelectedItem as ProjectorTable).PORT, "0");
                    mainwindow.deleteFromDictionary("PANASONIC" + (listView.SelectedItem as ProjectorTable).PORT);
                    portCmbx.Items.Add((listView.SelectedItem as ProjectorTable).PORT);
                    listView.Items.Remove((listView.SelectedItem as ProjectorTable));
                }

                else if ((listView.SelectedItem as ProjectorTable).ProjectorType == "DIGITALPROJECTION")
                {
                    ini.writeString("PROJECTORS", "DIGITALPROJECTION" + (listView.SelectedItem as ProjectorTable).PORT, "0");
                    mainwindow.deleteFromDictionary("DIGITALPROJECTION" + (listView.SelectedItem as ProjectorTable).PORT);
                    portCmbx.Items.Add((listView.SelectedItem as ProjectorTable).PORT);
                    listView.Items.Remove((listView.SelectedItem as ProjectorTable));
                }

                else if ((listView.SelectedItem as ProjectorTable).ProjectorType == "EPSON")
                {
                    ini.writeString("PROJECTORS", "EPSON" + (listView.SelectedItem as ProjectorTable).PORT, "0");
                    mainwindow.deleteFromDictionary("EPSON" + (listView.SelectedItem as ProjectorTable).PORT);
                    portCmbx.Items.Add((listView.SelectedItem as ProjectorTable).PORT);
                    listView.Items.Remove((listView.SelectedItem as ProjectorTable));
                }

                else if ((listView.SelectedItem as ProjectorTable).ProjectorType == "BENQ")
                {
                    ini.writeString("PROJECTORS", "BENQ" + (listView.SelectedItem as ProjectorTable).PORT, "0");
                    mainwindow.deleteFromDictionary("BENQ" + (listView.SelectedItem as ProjectorTable).PORT);
                    portCmbx.Items.Add((listView.SelectedItem as ProjectorTable).PORT);
                    listView.Items.Remove((listView.SelectedItem as ProjectorTable));
                }
            }

        }
        private void statusBtn_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedItem != null)
            {
                try
                {
                    MessageBox.Show("Status: " + mainwindow.getDictionaryValue((listView.SelectedItem as ProjectorTable).ProjectorType + (listView.SelectedItem as ProjectorTable).PORT).GetStatus() + "\nLamp Hours: " + mainwindow.getDictionaryValue((listView.SelectedItem as ProjectorTable).ProjectorType + (listView.SelectedItem as ProjectorTable).PORT).GetLampHours());
                }
                catch
                {

                }
            }
        }
        private void turnOffBtn_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedItem != null)
                mainwindow.getDictionaryValue((listView.SelectedItem as ProjectorTable).ProjectorType + (listView.SelectedItem as ProjectorTable).PORT).TurnOff();
        }
        private void turnOnBtn_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedItem != null)
                mainwindow.getDictionaryValue((listView.SelectedItem as ProjectorTable).ProjectorType + (listView.SelectedItem as ProjectorTable).PORT).TurnOn();
        }
        private void timeIntervalCmbx_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IniFile file = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");

            if (((ComboBox)sender).SelectedItem.ToString() == "15 MIN")
                file.writeInt("SETTINGS", "interval", 15);
            if (((ComboBox)sender).SelectedItem.ToString() == "20 MIN")
                file.writeInt("SETTINGS", "interval", 20);
            if (((ComboBox)sender).SelectedItem.ToString() == "30 MIN")
                file.writeInt("SETTINGS", "interval", 30);
            if (((ComboBox)sender).SelectedItem.ToString() == "60 MIN")
                file.writeInt("SETTINGS", "interval", 60);

        }
        private void scheduleStartCmbx_selectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IniFile file = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");

            string starthour = ((ComboBox)sender).SelectedItem.ToString();
            if (starthour == "07:00")
                file.writeInt("SETTINGS", "startTime", 7);
            if (starthour == "08:00")
                file.writeInt("SETTINGS", "startTime", 8);
            if (starthour == "09:00")
                file.writeInt("SETTINGS", "startTime", 9);
            if (starthour == "10:00")
                file.writeInt("SETTINGS", "startTime", 10);
            if (starthour == "11:00")
                file.writeInt("SETTINGS", "startTime", 11);
            if (starthour == "12:00")
                file.writeInt("SETTINGS", "startTime", 12);
            if (starthour == "13:00")
                file.writeInt("SETTINGS", "startTime", 13);
            if (starthour == "14:00")
                file.writeInt("SETTINGS", "startTime", 14);
            if (starthour == "15:00")
                file.writeInt("SETTINGS", "startTime", 15);

        }
        private void textbox_previewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;

        }
        private void settingPage_Unloaded(object sender, RoutedEventArgs e)
        {
            updateRebootTime();
        }
        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsChecked == true)
            {
                ini.writeBool("SETTINGS", "rebootIsChecked", true);
            }
            else
            {
                ini.writeBool("SETTINGS", "rebootIsChecked", false);
            }
        }

        private void addProjectorToTable(string name)
        {
            listView.Items.Add(new ProjectorTable { ProjectorType = name, PORT = (string)portCmbx.SelectedItem });
            Projector projector = null;

            switch (name)
            {
                case "BENQ":
                    projector = new BENQ((string)portCmbx.SelectedItem);
                    break;
                case "DIGITALPROJECTION":
                    projector = new BENQ((string)portCmbx.SelectedItem);
                    break;
                case "EPSON":
                    projector = new Epson((string)portCmbx.SelectedItem);
                    break;
                case "PANASONIC":
                    projector = new Panasonic((string)portCmbx.SelectedItem);
                    break;
                case "SANYO":
                    projector = new Sanyo((string)portCmbx.SelectedItem);
                    break;
                case "SHARP":
                    projector = new Sharp((string)portCmbx.SelectedItem);
                    break;
            }
            if (projector != null)
                mainwindow.addToDictionary(name + (string)portCmbx.SelectedItem, projector);
            ini.writeString("PROJECTORS", name + (string)portCmbx.SelectedItem, "1");
            portCmbx.Items.Remove((string)portCmbx.SelectedItem);

        }
        private void intializeProjectorTypeCmbx()
        {
            projectorTypeCmbx.Items.Add("BENQ");
            projectorTypeCmbx.Items.Add("DIGITALPROJECTION");
            projectorTypeCmbx.Items.Add("EPSON");
            projectorTypeCmbx.Items.Add("PANASONIC");
            projectorTypeCmbx.Items.Add("SANYO");
            projectorTypeCmbx.Items.Add("SHARP");

        }
        private void intializeScheduleCmbx()
        {
            scheduleStartCmbx.Items.Add("07:00");
            scheduleStartCmbx.Items.Add("08:00");
            scheduleStartCmbx.Items.Add("09:00");
            scheduleStartCmbx.Items.Add("10:00");
            scheduleStartCmbx.Items.Add("11:00");
            scheduleStartCmbx.Items.Add("12:00");
            scheduleStartCmbx.Items.Add("13:00");
            scheduleStartCmbx.Items.Add("14:00");
            scheduleStartCmbx.Items.Add("15:00");
        }
        private void intializeTimeIntervalCmbx()
        {
            //timeIntervalCmbx.Items.Add("15 MIN");
            //timeIntervalCmbx.Items.Add("20 MIN");
            timeIntervalCmbx.Items.Add("30 MIN");
            timeIntervalCmbx.Items.Add("60 MIN");
        }
        private void intializePortCmbx()
        {
            string[] portnames = SerialPort.GetPortNames();
            foreach (var item in portnames)
                portCmbx.Items.Add(item);
        }
        private void initializeProjectorTable()
        {
            ini = new IniFile(@"C:\BEAM\Projectors\Settings\projectorScheduling.ini");
            string[] names = { "BENQ", "DIGITALPROJECTION", "EPSON", "PANASONIC", "SANYO", "SHARP", };
            string projector;
            var bla = mainwindow.getNumOfProjectors();

            for (int i = 0; i < names.Length; i++)
            {
                for (int j = 0; j < 29; j++)
                {
                    projector = ini.readString("PROJECTORS", names[i] + "COM" + j, "0");
                    if (projector == "1")
                    {
                        if (names[i] == "BENQ")
                            mainwindow.addToDictionary(names[i] + "COM" + j, new BENQ("COM" + j));

                        else if (names[i] == "DIGITALPROJECTION")
                            mainwindow.addToDictionary(names[i] + "COM" + j, new DigitalProjection("COM" + j));

                        else if (names[i] == "EPSON")
                            mainwindow.addToDictionary(names[i] + "COM" + j, new Epson("COM" + j));

                        else if (names[i] == "PANASONIC")
                            mainwindow.addToDictionary(names[i] + "COM" + j, new Panasonic("COM" + j));

                        else if (names[i] == "SANYO")
                            mainwindow.addToDictionary(names[i] + "COM" + j, new Sanyo("COM" + j));

                        else
                            mainwindow.addToDictionary(names[i] + "COM" + j, new Sharp("COM" + j));

                        int COUNT = mainwindow.getNumOfProjectors();
                        if (listView.Items.Count < mainwindow.getNumOfProjectors())
                        {
                            listView.Items.Add(new ProjectorTable { ProjectorType = names[i], PORT = "COM" + j });
                            portCmbx.Items.Remove("COM" + j);
                        }
                    }

                }

            }



        }
      
        public string getInterval()
        {
            return timeIntervalCmbx.SelectedItem.ToString();
        }
        public void updateRebootTime()
        {
            ini.writeString("SETTINGS", "rebootHour", textBox1.Text);
            ini.writeString("SETTINGS", "rebootMinute", textBox2.Text);
        }

        private void updateTable()
        {
            string[] portnames = SerialPort.GetPortNames();
            portCmbx.Items.Clear();

            foreach (var port in portnames)
            {  
                bool exist_flag = false;

                foreach (var item in listView.Items)
                {
                    if (item.ToString().Equals(port.ToString()))
                        exist_flag = true;
                }

                if (!exist_flag)
                    portCmbx.Items.Add(port);
            }
        }

        private void DropDownOpened(object sender, EventArgs e)
        {
            string[] portnames = SerialPort.GetPortNames();
            if (portnames.Length > portCmbx.Items.Count + listView.Items.Count || portnames.Length + listView.Items.Count < portCmbx.Items.Count)
                updateTable();
            
        }
    }

    public class ProjectorTable
    {
        public string ProjectorType { get; set; }
        public string PORT { get; set; }
    }
}
