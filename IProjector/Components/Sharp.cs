﻿namespace Projectors.Components
{
    public class Sharp : Projector
    {
        public Sharp(string portName)
        {
            this.portName = portName;
            this.Type = ProjectorType.SHARP;
            this.TURN_ON = buildSharpPrefix() + " ? 00   POWR   125CM" + (char)0x0D;
            this.TURN_OFF = buildSharpPrefix() + " ? 00   POWR   024CM" + (char)0x0D;
            this.BPS = 9600;

        }

        private static string buildSharpPrefix()
        {
            string r = "";
            for (int i = 0; i < 34; i++)
                r = r + (char)223;
            return r;
        }
    }
}
