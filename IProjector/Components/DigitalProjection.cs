﻿namespace Projectors.Components
{
    public class DigitalProjection : Projector
    {
        public DigitalProjection(string portName)
        {
            this.portName = portName;
            this.Type = ProjectorType.DIGITALPROJECTION;

            this.TURN_ON = "" + (char)2 + (char)0 + (char)0 + (char)0 + (char)0 + (char)2;
            this.TURN_OFF = "" + (char)2 + (char)1 + (char)0 + (char)0 + (char)0 + (char)3;
            this.STATUS_READ = "" + (char)0 + (char)85 + (char)0 + (char)0 + (char)1 + (char)1 + (char)87;

            this.BPS = 38400;
        }
    }
}
