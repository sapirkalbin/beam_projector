﻿namespace Projectors.Components
{
    public class BENQ : Projector
    {
        public BENQ(string portName)
        {
            this.Type = ProjectorType.BENQ;
            this.portName = portName;

            this.TURN_ON = (char)0x0D + "*pow=on#" + (char)0x0D;
            this.TURN_OFF = (char)0x0D + "*pow=off#" + (char)0x0D;
            this.STATUS_READ = (char)0x0D + "*pow=?#" + (char)0x0D;
            this.LAMP_READ = (char)0x0D + "*ltim=?#" + (char)0x0D;
            this.BPS = 115200;
        }
    }
}
