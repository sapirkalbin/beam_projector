﻿namespace Projectors.Components
{
    public class Epson : Projector
    {
        public Epson(string portName)
        {
            this.portName = portName;
            this.Type = ProjectorType.EPSON; 

            this.TURN_ON = "PWR ON" + (char)0x0D;
            this.TURN_OFF = "PWR OFF" + (char)0x0D;
            this.STATUS_READ = "PWR?" + (char)0x0D;
            this.LAMP_READ = "LAMP?" + (char)0x0D;
            this.BPS = 9600;
        }


    }
}