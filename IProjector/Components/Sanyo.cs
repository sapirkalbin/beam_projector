﻿namespace Projectors.Components
{
    public class Sanyo : Projector
    {
        public Sanyo(string portName)
        {
            this.portName = portName;
            this.Type = ProjectorType.SANYO;
            this.TURN_ON = (char)0x0D + "C00" + (char)0x0D;
            this.TURN_OFF = (char)0x0D + "C01" + (char)0x0D;
            this.STATUS_READ = "CR0" + (char)0x0D;
            this.BPS = 19200;
        }
    }
}
