﻿namespace Projectors.Components
{
    public class Panasonic : Projector
    {
        public Panasonic(string portName)
        {
            this.portName = portName;
            this.Type = ProjectorType.PANASONIC;
            
            this.TURN_ON = "" + (char)0x02 + (char)0x00 + "PON" + (char)0x03;
            this.TURN_OFF = "" + (char)0x02 + (char)0x00 + "POF" + (char)0x03;
            this.STATUS_READ = "" + (char)0x02 + (char)0x00 + (char)0xfe + (char)0x03;
            this.BPS = 9600;
        }
    }
}
