﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using System.Collections.Generic;
/// <summary>
/// Create a New INI file to store or load data
/// </summary>
public class IniFile
{
    public string path;

    [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, string lpReturnString, int nSize, string lpFilename);

    [DllImport("kernel32", EntryPoint = "GetPrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


    //[DllImport("kernel32")]
    //private static extern long WritePrivateProfileString(string section,string key,string val,string filePath);
    [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern int WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFilename);

    [DllImport("kernel32")]
    private static extern long WritePrivateProfileSection(string section,
        string keysAndValues, string filePath);



    [DllImport("kernel32")]
    private static extern int GetPrivateProfileSection(string section,
        IntPtr keysAndValues, int bufferSize, string filePath);




    /// <summary>
    /// INIFile Constructor.
    /// </summary>
    /// <PARAM name="INIPath"></PARAM>
    public IniFile(string INIPath)
    {
        path = INIPath;
        System.IO.FileInfo fileInfo = new System.IO.FileInfo(path);
        if (!fileInfo.Exists)
        {
            FileStream fs = fileInfo.OpenWrite();
            fs.Close();
        }
        // Add full path if needed
        if (path.LastIndexOf("\\") < 0)
            path = fileInfo.DirectoryName + '\\' + path;
    }
    /// <summary>
    /// Write Data to the INI File
    /// </summary>
    /// <PARAM name="Section"></PARAM>
    /// Section name
    /// <PARAM name="Key"></PARAM>
    /// Key Name
    /// <PARAM name="Value"></PARAM>
    /// Value Name
    public void writeValue(string Section, string Key, string Value)
    {
        WritePrivateProfileString(Section, Key, Value, this.path);
    }

    public void writeString(string Section, string Key, string Value)
    {
        writeValue(Section, Key, Value);
    }

    public void writeInt(string Section, string Key, int Value)
    {
        writeValue(Section, Key, Value.ToString());
    }

    public void writeLong(string Section, string Key, long Value)
    {
        writeValue(Section, Key, Value.ToString());
    }

    public void writeBool(string Section, string Key, bool Value)
    {
        writeValue(Section, Key, Value.ToString());
    }

    /// <summary>
    /// Read Data Value From the Ini File
    /// </summary>
    /// <PARAM name="Section"></PARAM>
    /// <PARAM name="Key"></PARAM>
    /// <PARAM name="Path"></PARAM>
    /// <returns></returns>
    public string readValue(string Section, string Key, string defaultStr)
    {
        StringBuilder temp = new StringBuilder(255);
        int i = GetPrivateProfileString(Section, Key, defaultStr, temp,
            255, this.path);
        return temp.ToString();

    }

    public string readString(string Section, string Key, string defaultStr)
    {
        return readValue(Section, Key, defaultStr);
    }


    public int readInt(string Section, string Key, int defaultVal)
    {

        string resStr = readValue(Section, Key, defaultVal.ToString());
        int res = defaultVal;
        try
        {
            res = int.Parse(resStr);
        }
        catch (System.Exception)
        {
        }
        return res;
    }
    public long readLong(string Section, string Key, long defaultVal)
    {

        string resStr = readValue(Section, Key, defaultVal.ToString());
        long res = defaultVal;
        try
        {
            res = long.Parse(resStr);
        }
        catch (System.Exception)
        {
        }
        return res;
    }

    public bool readBool(string Section, string Key, bool defaultVal)
    {
        string resStr = readValue(Section, Key, defaultVal.ToString());
        bool res = defaultVal;
        try
        {
            res = bool.Parse(resStr);
        }
        catch (System.Exception)
        {
        }
        return res;
    }

    public float readFloat(string Section, string Key, float defaultVal)
    {
        string resStr = readValue(Section, Key, defaultVal.ToString());
        float res = defaultVal;
        Single.TryParse(resStr, out res);
        return res;
    }


    /// <summary>
    /// Reads a whole section of the INI file.
    /// </summary>
    /// <param name="section">Section to read.</param>
    /*public string[] readSection(string section)
    {
        const int bufferSize = 2048;

        StringBuilder returnedString = new StringBuilder(bufferSize);
					   
        GetPrivateProfileSection(section, returnedString, bufferSize, this.path);
		  
        string sectionData = returnedString.ToString();
        return sectionData.Split('\0');
    }*/

    //TOD: remove this function and to use the functions: GetCategories, GetKeys
    public string[] readSection(string section)
    {
        const int bufferSize = 2048;

        StringBuilder returnedString = new StringBuilder();

        IntPtr pReturnedString = Marshal.AllocCoTaskMem(bufferSize);
        try
        {
            int bytesReturned = GetPrivateProfileSection(section, pReturnedString, bufferSize, this.path);

            //bytesReturned -1 to remove trailing \0
            for (int i = 0; i < bytesReturned - 1; i++)
                returnedString.Append((char)Marshal.ReadByte(new IntPtr((uint)pReturnedString + (uint)i)));
        }
        finally
        {
            Marshal.FreeCoTaskMem(pReturnedString);
        }

        string sectionData = returnedString.ToString();
        return sectionData.Split('\0');
    }


    /// <summary>
    /// Writes a whole section of the INI file.
    /// </summary>
    /// <param name="section">Section to read.</param>
    /// <param name="keysAndValues">Array of key=value pairs.</param>
    public void writeSection(string section, string[] keysAndValues)
    {
        StringBuilder sectionData = new StringBuilder();

        foreach (string currPair in keysAndValues)
        {
            sectionData.AppendLine(currPair);
        }

        WritePrivateProfileSection(section, sectionData.ToString(), this.path);
    }

    // <summary>
    /// Reset a section
    /// </summary>
    /// <PARAM name="Section"></PARAM>
    /// Section name      
    public void resetSection(string Section)
    {
        WritePrivateProfileSection(Section, null, this.path);
    }

    public List<string> GetCategories()
    {
        string returnString = new string(' ', 65536);
        GetPrivateProfileString(null, null, null, returnString, 65536, this.path);
        List<string> result = new List<string>(returnString.Split('\0'));
        result.RemoveRange(result.Count - 2, 2);
        return result;
    }

    public List<string> GetKeys(string category)
    {
        string returnString = new string(' ', 32768);
        GetPrivateProfileString(category, null, null, returnString, 32768, this.path);
        List<string> result = new List<string>(returnString.Split('\0'));
        result.RemoveRange(result.Count - 2, 2);
        return result;
    }
}
