﻿using System;
using System.IO;
using System.IO.Ports;
using System.Threading;

namespace Projectors
{
    public abstract class Projector
    {
        private SerialPort serialPort = new SerialPort();
        public enum ProjectorType : int
        {
            SANYO,
            DIGITALPROJECTION,
            SHARP,
            PANASONIC,
            BENQ,
            EPSON
        }

        protected ProjectorType Type;
        protected string portName;
        protected string TURN_ON, TURN_OFF, STATUS_READ, LAMP_READ;
        protected int BPS;

        public string getProjectorType()
        {
            return this.Type.ToString();
        }
        public string getPort()
        {
            return this.portName;
        }

        public void TurnOn()
        {
            try
            {
                serialPort = initPort();
                sendCommand(TURN_ON, BPS);
                if (Type == ProjectorType.SANYO)
                    sendCommand(TURN_ON, BPS);

                writeLog("turned on");

            }
            catch (Exception e)
            {
                writeLog("Failed to turn on projector:" + e.Message);
                if (serialPort.IsOpen) serialPort.Close();

            }
        }
        public void TurnOff()
        {
            try
            {
                serialPort = initPort();

                sendCommand(TURN_OFF, BPS);
                sendCommand(TURN_OFF, BPS);
                sendCommand(TURN_OFF, BPS);

                writeLog("turned off");
            }
            catch (Exception e)
            {
                writeLog("Failed to turn off projector:" + e.Message);
                if (serialPort.IsOpen) serialPort.Close();
            }

        }
        public string GetLampHours()
        {
            try
            {
                serialPort = initPort();
                String res = sendCommand(this.LAMP_READ, this.BPS);
                if (this.getProjectorType() == "EPSON")
                    return EPSONcleanAnswer(res);
                else if (this.getProjectorType() == "BENQ")
                    return BENQcleanAnswer(res);
                else
                    return cleanAnswer(res);

            }

            catch (Exception e)
            {
                writeLog("Failed to turn on projector:" + e.Message);
                if (serialPort.IsOpen) serialPort.Close();
                return null;
            }
        }
        public string GetStatus()
        {
            try
            {
                serialPort = initPort();
                String res = sendCommand(this.STATUS_READ, this.BPS);
                if (this.getProjectorType() == "EPSON")
                    return EPSONcleanAnswer(res);
                else if (this.getProjectorType() == "BENQ")
                    return BENQcleanAnswer(res);
                else
                    return cleanAnswer(res);
            }

            catch (Exception e)
            {
                writeLog("Failed to turn on projector:" + e.Message);
                if (serialPort.IsOpen) serialPort.Close();
                return null;
            }
        }

        private string sendCommand(string msg, int speed)
        {
            string r = null;
            if (serialPort.IsOpen) serialPort.Close();
            serialPort.BaudRate = speed;
            serialPort.Open();
            serialPort.Write(msg);
            Thread.Sleep(1000);
            r = serialPort.ReadExisting();
            serialPort.Close();

            return r;
        }
        private SerialPort initPort()
        {
            SerialPort sp = new SerialPort();
            sp.Parity = Parity.None;
            sp.StopBits = StopBits.One;
            sp.DataBits = 8;
            sp.ReadTimeout = 1000;
            sp.PortName = this.portName;
            return sp;
        }
        private string cleanAnswer(string str)
        {
            int len = str.Length;
            return str.Substring(str.LastIndexOf("=") + 1, len - str.LastIndexOf("=") - 1);
        }
        private string BENQcleanAnswer(string str)
        {
            int len = str.Length;
            return str.Substring(str.LastIndexOf("=") + 1, len - str.LastIndexOf("=") - 4);
        }
        private string EPSONcleanAnswer(string str)
        {
            int len = str.Length;
            var result = str.Substring(str.LastIndexOf("=") + 1, len - str.LastIndexOf("=") - 3);
            if (result == "00")
                return result + " - Standby";
            else if (result == "01")
                return result + " - Power on";
            else if (result == "02")
                return result + " - Warm up";
            else if (result == "03")
                return result + " - Cooling down";
            else if (result == "04")
                return result + " - Standby";
            else
                return result;
        }
        public void writeLog(string message)
        {
            File.AppendAllText("C:/BEAM/Projectors/Settings/log.txt", DateTime.Now.Date.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToString("HH:mm:ss") + " :" + message + "\n");
        }

    }



}

